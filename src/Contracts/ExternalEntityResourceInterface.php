<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Contracts;

/**
 * Define the external entity resource interface.
 */
interface ExternalEntityResourceInterface {

  /**
   * Get external entity resource properties.
   *
   * @return array
   *   An array of properties.
   */
  public function getProperties(): array;

  /**
   * Get external entity resource target entity bundle.
   *
   * @return string
   *   Return the entity bundle.
   */
  public function getTargetEntityBundle(): ?string;

  /**
   * Get external entity resource target entity type ID.
   *
   * @return string
   *   Return the entity type ID.
   */
  public function getTargetEntityTypeId(): ?string;

  /**
   * Get all external entity resource properties.
   *
   * @return array
   *   An array of the external entity resource
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllProperties(): array;

  /**
   * Define the default resource properties.
   *
   * @return string[]
   *   An array of property ids.
   */
  public function defaultPropertyIds(): array;

  /**
   * Get external entity resource dependencies.
   *
   * @return array
   *   An array of resource dependencies.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getResourceDependencies(): array;

  /**
   * Get external entity resource dependency status.
   *
   * @return array
   *   An array of the resource dependency status.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getResourceDependencyStatus(): array;

  /**
   * Get all external entity property field definitions.
   *
   * @param string|null $interface = NULL
   *   Filter properties by a defined interface.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPropertyFieldDefinitions(string $interface = NULL): array;

  /**
   * Check if the entity resource exist in the storage.
   *
   * @return bool
   *   Return TRUE if resource exist already; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(): bool;

}
