<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Contracts;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Define external entity cache invalidator interface.
 */
interface ExternalEntityCacheInvalidatorInterface extends ConfigEntityInterface {

  /**
   * Get the cache invalidator resource domain raw.
   *
   * @return string|null
   *   The resource domain as a formatted string.
   */
  public function domain(): ?string;

  /**
   * Get the cache invalidator resource information.
   *
   * @return array
   *   An array of the resource information.
   */
  public function resource(): array;

  /**
   * Get the cache invalidator resource entity types.
   *
   * @return array
   *   An array of resource entity types.
   */
  public function resourceEntityTypes(): array;

  /**
   * Get the cache invalidator domain list.
   *
   * @return array
   *   An array of invalidator domains.
   */
  public function getDomainList(): array;
}
