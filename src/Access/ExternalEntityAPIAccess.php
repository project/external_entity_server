<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Define the external entity API access class.
 */
class ExternalEntityAPIAccess implements AccessInterface {

  /**
   * External entity resource authentication.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configuration;

  /**
   * The access constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configuration = $config_factory->get(
      'external_entity_server.resource.authentication'
    );
  }

  /**
   * Check access for the external entity resources.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account instance.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result object.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    if ($this->configuration->get('anonymous_access')) {
      return AccessResult::allowed();
    }

    return AccessResult::allowedIf(
      $account->isAuthenticated()
      && $account->hasPermission('view external entity api resources')
    );
  }

}
