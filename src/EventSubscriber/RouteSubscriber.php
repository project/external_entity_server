<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Define the external entity server route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The route subscriber constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get(
      'external_entity_server.resource.authentication'
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if (
      !$this->config->get('anonymous_access')
      && ($provider = $this->config->get('provider'))
    ) {
      if (!is_array($provider)) {
        $provider = [$provider];
      }
      $routes = [
        'external_entity_server.status',
        'external_entity_server.resource',
        'external_entity_server.resource.info',
        'external_entity_server.definition.lookup',
        'external_entity_server.definition.search'
      ];

      foreach ($routes as $route_name) {
        if ($route = $collection->get($route_name)) {
          $route->setOption('_auth', array_values($provider));
        }
      }
    }
  }

}
