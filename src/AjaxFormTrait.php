<?php

declare(strict_types=1);

namespace Drupal\external_entity_server;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Define a reusable AJAX form trait.
 */
trait AjaxFormTrait {

  /**
   * AJAX form element callback.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   An array form elements.
   */
  public function ajaxFormElementCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    $element = $form_state->getTriggeringElement();

    if (!isset($element['#ajax_element'])) {
      return $form;
    }
    $ajax_element = $element['#ajax_element'];

    if (!is_array($ajax_element)) {
      $ajax_element = [$ajax_element];
    }

    return NestedArray::getValue($form, $ajax_element);
  }

  /**
   * AJAX form depth callback.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form elements for the provided depth.
   */
  public function ajaxFormDepthCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    $element = $form_state->getTriggeringElement();
    $depth = $element['#depth'] ?? 1;

    return NestedArray::getValue(
      $form,
      array_slice($element['#array_parents'], 0, (int) "-{$depth}")
    );
  }

  /**
   * Get form state value.
   *
   * @param string|array $key
   *   The form state key.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param mixed $default_value
   *   The form state default value.
   *
   * @return mixed
   *   The form state value.
   */
  protected function getFormStateValue(
    $key,
    FormStateInterface $form_state,
    $default_value = NULL
  ) {
    $key = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput()
    ];

    foreach ($inputs as $input) {
      $key_exists = NULL;
      $value = NestedArray::getValue($input, $key, $key_exists);

      if ($key_exists) {
        return $value;
      }
    }

    return $default_value;
  }

}
