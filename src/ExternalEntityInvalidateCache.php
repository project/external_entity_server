<?php

declare(strict_types=1);

namespace Drupal\external_entity_server;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * External entity invalidate cache service.
 */
class ExternalEntityInvalidateCache {

  /**
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * @var array
   */
  protected $invalidators = [];

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $cacheInvalidatorStorage;

  /**
   * Define the external entity cache invalidator URL path.
   */
  protected const CACHE_INVALIDATOR_PATH = 'external-entity/api/cache/invalidator';

  /**
   * Define the class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    ClientFactory $client_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->httpClient = $client_factory->fromOptions();
    $this->entityFieldManager = $entity_field_manager;
    $this->cacheInvalidatorStorage = $entity_type_manager
      ->getStorage('external_entity_cache');
  }

  /**
   * Invalidate external entity based on host entity.
   */
  public function invalidateEntity(EntityInterface $entity, string $action): void {
    if ($invalidators = $this->cacheInvalidators($entity->getEntityTypeId(), $entity->bundle())) {
      $payload = [
        'action' => $action,
      ];
      $url_path = static::CACHE_INVALIDATOR_PATH;

      foreach ($invalidators as $invalidator) {
        $cache_tags = $this->extractExternalEntityCacheIds(
          $entity,
          $invalidator->resourceEntityTypes()
        );

        foreach ($invalidator->getDomainList() as $domain) {
          $url = "{$this->normalizeUrl($domain)}/{$url_path}";
          if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            continue;
          }
          try {
            $this->httpClient->post($url, [
              'headers' => [
                'content-type' => 'application/json',
              ],
              'json' => [
                  'cache_tags' => $cache_tags
                ] + $payload,
            ]);
          } catch (\Exception $exception) {
            watchdog_exception('external_entity_server', $exception);
          }
        }
      }
    }
  }

  /**
   * Create an external entity cache ID.
   */
  protected function createExternalEntityCacheId(
    EntityInterface $entity
  ): string {
    return "external_entity:{$entity->uuid()}";
  }

  /**
   * Extract external entity cache IDs.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content entity.
   * @param array $entity_types
   *   An array of entity types that are allowed.
   *
   * @return array
   *   An array of external entity cache IDs.
   */
  protected function extractExternalEntityCacheIds(
    EntityInterface $entity,
    array $entity_types
  ): array {
    $cache_ids = [];

    foreach ($entity->getFields() as $field) {
      if (
        !$field instanceof EntityReferenceFieldItemListInterface
        || !$field->entity instanceof EntityInterface
      ) {
        continue;
      }
      $entity = $field->entity;
      $lookup_id = "{$entity->getEntityTypeId()}:{$entity->bundle()}";

      if (in_array($lookup_id, $entity_types, TRUE)) {
        $cache_ids[] = $this->createExternalEntityCacheId($entity);

        foreach ($field->referencedEntities() as $referenced) {
          $cache_ids[] = $this->createExternalEntityCacheId($referenced);
        }
      }
    }

    return array_unique($cache_ids);
  }

  /**
   * Normalize URL string.
   *
   * @param string $url
   *   The URL not normalize.
   *
   * @return string
   *   The normalized URL.
   */
  protected function normalizeUrl(string $url): string {
    return strrpos($url, '/') + 1 === strlen($url)
      ? rtrim($url, '/')
      : $url;
  }

  /**
   * External entity cache invalidators for a given entity type/bundle.
   *
   * @param string $entity_type
   *   The entity type id.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator[]
   *   An array of cache invalidators for an entity type and bundle.
   */
  protected function cacheInvalidators(
    string $entity_type,
    string $bundle
  ): array {
    return $this->loadCacheInvalidators()["{$entity_type}:{$bundle}"] ?? [];
  }

  /**
   * Load external entity cache invalidators
   *
   * @return \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator[]
   *   An array of cache invalidators keyed by the entity type/bundle.
   */
  protected function loadCacheInvalidators(): array {
    if (!isset($this->invalidators) || empty($this->invalidators)) {
      /** @var \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator $invalidator */
      foreach ($this->cacheInvalidatorStorage->loadMultiple() as $invalidator) {
        foreach ($invalidator->resourceEntityTypes() as $type) {
          $this->invalidators[$type][] = $invalidator;
        }
      }
    }

    return $this->invalidators;
  }

}
