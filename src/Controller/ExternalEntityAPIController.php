<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\external_entity_server\Contracts\ExternalEntityResourceInterface;

/**
 * Define the external entity server API controller.
 */
class ExternalEntityAPIController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * @var array
   */
  protected $queryInfo = [];

  /**
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   *  Define the default page limit.
   */
  protected const DEFAULT_PAGE_LIMIT = 50;

  /**
   * Define the external entity API version.
   */
  protected const API_VERSION = '1.0.0';

  /**
   * Remote entities definitions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(
    RequestStack $request_stack,
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager
  ) {
    $this->request = $request_stack->getCurrentRequest();
    $this->entityRepository = $entity_repository;
    $this->fieldTypeManager = $field_type_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('request_stack'),
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function status(): Response {
    return new JsonResponse([
      'status' => 'connected',
      'version' => static::API_VERSION,
    ]);
  }

  /**
   * Show all external entity resources.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *    A JSON representation of all defined resources.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function resource(): Response {
    return new JsonResponse(
      $this->getExternalEntityResources()
    );
  }

  /**
   * Show single external entity resource.
   *
   * @param string $entity_type_id
   *   The external entity type id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function resourceInfo(string $entity_type_id): Response {
    return new JsonResponse(
      $this->getExternalEntityResources()[$entity_type_id] ?? []
    );
  }

  /**
   * Search for the external entity.
   *
   * If the entity type is not passed along in the query string, then all
   * associated bundles for that resource are used instead.
   *
   * @param string|null $entity_type_id
   *   The entity type identifier.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return the response object.
   */
  public function search(string $entity_type_id = NULL): Response {
    $data = [];

    try {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $query = $storage->getQuery();

      $this->applyQueryRequestFilter($entity_type_id, $query);

      $data = [
        'info' => [
          'total' => (clone $query)->accessCheck()->count()->execute(),
        ],
        'results' => [],
      ];
      $this->applyQueryRequestSort($query);
      $this->applyQueryRequestRange($query);

      $ids = $query->accessCheck()->execute();
      $filter = $this->extractQueryInfo()['filter'];
      $bundles = $this->getExternalEntityResourceBundles($entity_type_id);

      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      foreach ($storage->loadMultiple($ids) as $entity) {
        if (!in_array($entity->bundle(), $bundles, TRUE)) {
          continue;
        }
        $additional = [];

        foreach (array_keys($filter) as $name) {
          if (!$entity->hasField($name)) {
            continue;
          }
          $additional['matches'][$name] = $entity->get($name)->getString();
        }

        $data['results'] += $this->buildExternalEntityDefinition(
          $entity,
          $additional
        );
      }
    } catch (\Exception $exception) {
      watchdog_exception('external_entity_server', $exception);
    }

    return new JsonResponse($data);
  }

  /**
   * Lookup the external entity.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return the response object.
   */
  public function lookup(string $entity_type_id = NULL): Response {
    $data = [];
    $uuids = $this->request->query->all()['entity_uuids'] ?? [];

    if (empty($uuids)) {
      throw new BadRequestHttpException(
        'Unable to process request due to missing uuids.'
      );
    }

    foreach ($uuids as $uuid) {
      try {
        $repository = $this->entityRepository;

        if ($entity = $repository->loadEntityByUuid($entity_type_id, $uuid)) {
          $data += $this->buildExternalEntityDefinition($entity);
        }
      } catch (\Exception $exception) {
        watchdog_exception('remote_entities', $exception);
      }
    }

    return new JsonResponse($data);
  }

  /**
   * Get external entity resources.
   *
   * @return array
   *   An array of the external entity resources.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityResources(): array {
    $resources = [];

    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $definition */
    foreach ($this->getExternalEntityResourceStorage()->loadMultiple() as $definition) {
      $this->formatExternalResource($resources, $definition);
    }

    return $resources;
  }

  /**
   * Format the external entity resource.
   *
   * @param array $output
   *   An array that the resource is appended too.
   * @param \Drupal\external_entity_server\Contracts\ExternalEntityResourceInterface $definition
   *   The external entity definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  protected function formatExternalResource(
    array &$output,
    ExternalEntityResourceInterface $definition
  ): void {
    $target_bundle = $definition->getTargetEntityBundle();
    $target_type_id = $definition->getTargetEntityTypeId();
    $target_entity_type = $this->entityTypeManager->getDefinition(
      $target_type_id
    );

    if (!isset($output[$target_type_id])) {
      $output[$target_type_id] = [
        'label' => $target_entity_type->getLabel(),
        'type' => $target_type_id,
        'variations' => [],
        'properties' => [],
        'status' => $definition->status(),
      ];
    }
    $output[$target_type_id]['variations'][$target_bundle] = [
      'label' => $target_entity_type->getLabel(),
    ];

    if ($bundle_entity_type = $target_entity_type->getBundleEntityType()) {
      $target_bundle_type = $this->entityTypeManager
        ->getStorage($bundle_entity_type)
        ->load($target_bundle);

      $output[$target_type_id]['variations'][$target_bundle] = [
        'label' => $target_bundle_type->label(),
      ];
    }

    /** @var \Drupal\field\Entity\FieldConfig|\Drupal\Core\Field\BaseFieldDefinition $field_definition */
    foreach ($definition->getPropertyFieldDefinitions() as $name => $field_definition) {
      if (!$field_definition instanceof FieldDefinitionInterface) {
        continue;
      }

      $output[$target_type_id]['properties'][$target_bundle][$name] = [
        'label' => $field_definition->getLabel(),
        'name' => $field_definition->getName(),
        'type' => $field_definition->getType(),
        'settings' => $field_definition->getSettings(),
        'properties' => $this->getFieldDefinitionProperties($field_definition),
      ];
    }
  }

  /**
   * Get field definition properties.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition instance.
   *
   * @return array
   *   An array of field definition properties.
   */
  protected function getFieldDefinitionProperties(
    FieldDefinitionInterface $field_definition
  ): array {
    $properties = [];
    $field_item = $field_definition->getItemDefinition();

    /** @var \Drupal\Core\TypedData\DataDefinitionInterface $definition */
    foreach ($field_item->getPropertyDefinitions() as $name => $definition) {
      if ($name === 'value' || $definition->isComputed()) {
        continue;
      }
      $properties[$name] = [
        'type' => $definition->getDataType(),
      ];
    }

    return $properties;
  }

  /**
   * Extract the HTTP query information.
   *
   * @return array
   *   An array of the query information.
   */
  protected function extractQueryInfo(): array {
    if (empty($this->queryInfo)) {
      $query = $this->request->query;
      $query_all = $query->all();
      $pager = $query_all['pager'] ?? [];

      $this->queryInfo = [
        'page' => $query_all['page'] ?? 1,
        'sort' => $query_all['sort'] ?? [],
        'range' => $query_all['range'] ?? [],
        'filter' => $query_all['filter'] ?? [],
        'limit' => $pager['limit'] ?? static::DEFAULT_PAGE_LIMIT,
      ];
    }

    return $this->queryInfo;
  }

  /**
   * Apply query request range.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The entity query object.
   */
  protected function applyQueryRequestRange(QueryInterface $query): void {
    $info = $this->extractQueryInfo();

    if ($range = $info['range']) {
      $query->range(
        $range['start'] ?? NULL,
        $range['length'] ?? NULL
      );
    }
  }

  /**
   * Apply query request sort.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The entity query object.
   */
  protected function applyQueryRequestSort(QueryInterface $query): void {
    $info = $this->extractQueryInfo();

    if (isset($info['sort'])) {
      foreach ($info['sort'] as $sort) {
        if (!isset($sort['source'])) {
          continue;
        }
        $query->sort($sort['source'], $sort['direction']);
      }
    }
  }

  /**
   * Apply query request filter.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The entity query object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function applyQueryRequestFilter(
    string $entity_type_id,
    QueryInterface $query
  ): void {
    $filter = $this->extractQueryInfo()['filter'] ?? [];
    $fields = $this->entityFieldManager->getFieldMap()[$entity_type_id] ?? [];

    $this->applyBundleTypeFilter($entity_type_id, $query, $filter);

    foreach ($filter as $field => $info) {
      $field_name = substr($field, 0, strrpos($field, '.') ?: strlen($field));
      if (!isset($info['value'], $fields[$field_name]['type'])) {
        continue;
      }
      $field_type = $fields[$field_name]['type'];

      if ($value = $this->processQueryFilterValue($info['value'], $field_type)) {
        $query->condition(
          $field,
          $value,
          $info['operator'] ?? '='
        );
      }
    }
  }

  /**
   * Process the query filter value.
   *
   * @param $value
   *   The query filter value.
   * @param string $field_type
   *   The query field type.
   *
   * @return mixed
   *    The processed value.
   */
  protected function processQueryFilterValue(
    $value,
    string $field_type
  ) {
    if (in_array($field_type, ['datetime', 'daterange'])) {
      if (!is_array($value)) {
        $value = [$value];
      }

      foreach ($value as &$timestamp) {
        $timestamp = DrupalDateTime::createFromTimestamp($timestamp)
          ->format(
            DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
            ['timezone' => 'UTC']
          );
      }

      if (count($value) === 1) {
        $value = reset($value);
      }
    }

    return $value;
  }

  /**
   * Apply the bundle type filter.
   *
   * @param array $filter
   *   An array of the query filters.
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The request query object.
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function applyBundleTypeFilter(
    string $entity_type_id,
    QueryInterface $query,
    array &$filter
  ): void {
    $bundle_key = $this->entityTypeManager
      ->getDefinition($entity_type_id)
      ->getKey('bundle');

    $bundles = $this->getExternalEntityResourceBundles($entity_type_id);

    $query->condition(
      $bundle_key,
      $filter[$bundle_key]['value'] ?? $bundles,
      $filter[$bundle_key]['operator'] ?? 'IN'
    );

    unset($filter[$bundle_key]);
  }

  /**
   * Get external entity resource.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return \Drupal\external_entity_server\Contracts\ExternalEntityResourceInterface
   *   The external entity resource
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityResource(
    string $entity_type_id,
    string $bundle
  ): ExternalEntityResourceInterface {
    return $this->getExternalEntityResourceStorage()
      ->load("$entity_type_id.$bundle");
  }

  /**
   * Get external entity resource bundles.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of the external entity resource bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityResourceBundles(string $entity_type_id): array {
    $bundles = [];

    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource[] $resources */
    $resources = $this->getExternalEntityResourceStorage()
      ->loadByProperties(['entity_type_id' => $entity_type_id]);

    foreach ($resources as $resource) {
      $bundles[] = $resource->getTargetEntityBundle();
    }

    return $bundles;
  }

  /**
   * Build the external entity definition.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity objects.
   *
   * @return array
   *   An array of the entity definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function buildExternalEntityDefinition(
    EntityInterface $entity,
    array $additional = []
  ): array {
    $definition = [];

    $entity_uuid = $entity->uuid();
    $entity_bundle = $entity->bundle();
    $entity_type_id = $entity->getEntityTypeId();

    if (isset($entity_uuid)) {
      $definition[$entity_uuid] = [
        'resource' => $entity_type_id,
        'variation' => $entity_bundle,
        'properties' => [],
      ] + $additional;

      if ($entity instanceof ContentEntityInterface) {
        $this->attachExposedPropertyDefinitions(
          $definition[$entity_uuid],
          $entity
        );

        if (
          $entity->hasLinkTemplate('canonical')
          && $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString()
        ) {
          $definition[$entity_uuid]['path'] = $url;
        }
      }
    }

    return $definition;
  }

  /**
   * Check if the field type is a reference.
   *
   * @param string $type
   *   An entity field type.
   *
   * @return bool
   *   Return TRUE if the field type is a reference; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function isFieldReference(string $type): bool {
    if (!$this->fieldTypeManager->hasDefinition($type)) {
      return FALSE;
    }
    $base = EntityReferenceItem::class;
    $class = $this->fieldTypeManager->getPluginClass($type);

    return $class === $base || is_subclass_of($class, $base);
  }

  /**
   * Attach exposed property definitions.
   *
   * @param array $definition
   *   The existing definition.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function attachExposedPropertyDefinitions(
    array &$definition,
    ContentEntityInterface $entity
  ): void {
    $fields = $entity->getFields();

    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $resource */
    $resource = $this->getExternalEntityResource(
      $entity->getEntityTypeId(),
      $entity->bundle()
    );

    foreach ($resource->getAllProperties() as $name => $property) {
      if (!isset($fields[$name])) {
        continue;
      }
      /** @var \Drupal\Core\Field\FieldItemList $items */
      $items = $fields[$name];
      $value = $this->processPropertyValue($items);
      $type = $items->getFieldDefinition()->getType();
      $label = $items->getDataDefinition()->getLabel();

      $property_key = $property['name'] ?? $name;
      $definition['properties'][$property_key] = [
        'type' => $type,
        'label' => $label,
        'value' => $value,
        'class' => get_class($items),
      ];
    }
  }

  /**
   * Process the external entity property value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field item instance.
   *
   * @return array
   *   An array of the processed field property value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function processPropertyValue(FieldItemListInterface $items): array {
    $value = $items->getValue();
    $type = $items->getFieldDefinition()->getType();

    if ($this->isFieldReference($type)) {
      $target_type = $items->getSetting('target_type');
      $target_storage = $this->entityTypeManager->getStorage($target_type);

      foreach ($value as &$reference) {
        if (!isset($reference['target_id'])) {
          continue;
        }
        $reference_entity = $target_storage->load($reference['target_id']);

        if (!$reference_entity instanceof EntityInterface) {
          continue;
        }
        $reference['target_type'] = $target_type;
        $reference['target_uuid'] = $reference_entity->uuid();
      }
    }

    if ($type === 'file_uri') {
      /** @var \Drupal\Core\StreamWrapper\StreamWrapperManager $stream_wrapper_manager */
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');
      foreach ($value as &$reference) {
        if (!isset($reference['value'])) {
          continue;
        }
        $reference['value'] = $stream_wrapper_manager
          ->getViaUri($reference['value'])
          ->getExternalUrl();
      }
    }

    return $value;
  }

  /**
   * Get external entity resource storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getExternalEntityResourceStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('external_entity_resource');
  }

}
