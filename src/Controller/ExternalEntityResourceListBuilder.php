<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Define the external entity resource list builder.
 */
class ExternalEntityResourceListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Entity Type'),
      $this->t('Entity Bundle'),
      $this->t('Dependencies')
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    return [
      $entity->getTargetEntityTypeId(),
      $entity->getTargetEntityBundle(),
      $this->dependencyStatus($entity)
    ] + parent::buildRow($entity);
  }

  /**
   * Get external entity resource dependency status.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The resource entity.
   *
   * @return array[]
   *   An array of the resource dependency status.
   */
  protected function dependencyStatus(EntityInterface $entity): array {
    $data = [];
    $statuses = [
      'found' => $this->t('Found'),
      'missing' => $this->t('Missing'),
    ];
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $dependency_status = $entity->getResourceDependencyStatus();

    foreach ($statuses as $status => $title) {
      if ($found_items = $dependency_status[$status] ?? []) {
        $data[] = [
          '#theme' => 'item_list',
          '#title' => $title,
          '#items' => $found_items,
        ];
      }
    }

    if (empty($data)) {
      $data['#markup'] = $this->t(
        'No Required Dependencies'
      );
    }

    return ['data' => $data];
  }
}
