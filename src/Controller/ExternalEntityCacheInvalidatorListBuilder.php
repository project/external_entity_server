<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Define external entity cache invalidator list builder.
 */
class ExternalEntityCacheInvalidatorListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Label'),
      $this->t('Domains')
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator $entity */
    return [
      $entity->label(),
      $entity->domain(),
    ] + parent::buildRow($entity);
  }

}
