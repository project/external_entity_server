<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entity_server\AjaxFormTrait;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the external entity resource default form.
 */
class ExternalEntityResourceDefaultForm extends EntityForm {

  use AjaxFormTrait;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * The class constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $entity = $this->entity;
    $wrapper = 'external-entity-resource';

    $form['#prefix'] = "<div id='{$wrapper}'>";
    $form['#suffix'] = '</div>';

    $entity_type_id = $this->getFormStateValue(
      'entity_type_id',
      $form_state,
      $entity->getTargetEntityTypeId()
    );

    $form['entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#description' => $this->t('Select the entity type.'),
      '#options' => $this->getEntityTypeOptions(),
      '#default_value' => $entity_type_id,
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => $wrapper,
        'callback' => [$this, 'externalEntityAjaxCallback']
      ]
    ];

    if (isset($entity_type_id) && !empty($entity_type_id)) {
      $bundle = $this->getFormStateValue(
        'bundle',
        $form_state,
        $entity->getTargetEntityBundle()
      );
      $bundle_options = $this->getEntityBundleOptions($entity_type_id);

      $form['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#description' => $this->t('Select the bundles.'),
        '#options' => $bundle_options,
        '#empty_option' => $this->t('- Select -'),
        '#default_value' => $bundle,
        '#required' => TRUE,
        '#ajax' => [
          'event' => 'change',
          'method' => 'replace',
          'wrapper' => $wrapper,
          'callback' => [$this, 'externalEntityAjaxCallback']
        ]
      ];

      if (isset($bundle, $bundle_options[$bundle]) && !empty($bundle)) {
        $form['properties'] = [
          '#type' => 'table',
          '#title' => $this->t('Properties'),
          '#header' => [
            $this->t('Exposed'),
            $this->t('Field Label'),
            $this->t('Field Name'),
          ],
          '#empty' => $this->t('There are no entity properties available.')
        ];
        $properties = $entity->getProperties();
        $excluded_field_ids = $this->excludedFieldIds($entity_type_id);

        foreach ($this->getEntityTypeFieldDefinitions($entity_type_id, $bundle) as $definition) {
          $field_name = $definition->getName();

          if (in_array($field_name, $excluded_field_ids, TRUE)) {
            continue;
          }
          $label = $definition->getLabel();

          $form['properties'][$field_name]['exposed'] = [
            '#type' => 'checkbox',
            '#default_value' => $properties[$field_name]['exposed'] ?? FALSE,
          ];
          $form['properties'][$field_name]['label']['#markup'] = $label;
          $form['properties'][$field_name]['name'] = [
            '#type' => 'textfield',
            '#default_value' => $properties[$field_name]['name'] ?? $field_name,
            '#description' => $this->t(
              'Input the property name to use in the API response.'
            ),
            '#required' => TRUE,
          ];
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $entity = $this->entity;

    if ($entity->id() !== $entity->getOriginalId() && $entity->exists()) {
      $form_state->setError($form, $this->t(
        'There is already an external entity resource for @entity_type_id/@bundle.',
        [
          '@entity_type_id' => $entity->getTargetEntityTypeId(),
          '@bundle' => $entity->getTargetEntityBundle()
        ]
      ));
    }
  }

  /**
   * External entity Ajax callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   An array of form elements.
   */
  public function externalEntityAjaxCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function save(
    array $form,
    FormStateInterface $form_state
  ): int {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $entity = $this->entity;
    $status = $entity->save();

    $this->messenger()->addMessage(
      $this->t('The %label has been %action!', [
        '%label' => $entity->label(),
        '%action' => $status === SAVED_NEW ? 'created' : 'updated',
      ])
    );
    $form_state->setRedirectUrl($entity->toUrl('collection'));

    return $status;
  }

  /**
   * Exclude field IDs based on entity type ID.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   An array of field identifiers.
   */
  protected function excludedFieldIds(string $entity_type_id): array {
    $field_ids = [];
    try {
      /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
      $entity = $this->entity;
      $definition = $this->entityTypeManager->getDefinition($entity_type_id);

      $field_ids[] = $definition->getKey('bundle');

      foreach ($entity->defaultPropertyIds() as $key) {
        if ($key_value = $definition->getKey($key)) {
          $field_ids[] = $key_value;
        }
      }
    } catch (\Exception $exception) {
      watchdog_exception('external_entity_server', $exception);
    }

    return $field_ids;
  }

  /**
   * Get entity type options.
   *
   * @return array
   *   An array entity type options.
   */
  protected function getEntityTypeOptions(): array {
    $options = [];

    foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      if (!$definition instanceof ContentEntityTypeInterface) {
        continue;
      }
      $options[$id] = $definition->getLabel();
    }

    return $options;
  }

  /**
   * Get entity bundle options.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity bundle options.
   */
  protected function getEntityBundleOptions(string $entity_type_id): array {
    $options = [];

    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $name => $info) {
      if (!isset($info['label'])) {
        continue;
      }
      $options[$name] = $info['label'];
    }

    return $options;
  }

  /**
   * Get entity type field definitions.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $bundle
   *   The entity bundle.
   *
   * @return array
   *   An array of field definitions.
   */
  protected function getEntityTypeFieldDefinitions(
    string $entity_type_id,
    string $bundle
  ): array {
    return $this->entityFieldManager->getFieldDefinitions(
      $entity_type_id,
      $bundle
    );
  }

}
