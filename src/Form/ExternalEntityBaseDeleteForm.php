<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Define the external entity server base entity delete form.
 */
class ExternalEntityBaseDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritDoc}
   */
  public function getQuestion(): TranslatableMarkup {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $entity = $this->entity;

    return $this->t(
      'Are you sure you want to delete %name?', [
        '%name' => $entity->label()
      ]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl(): Url {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityResource $entity */
    $entity = $this->entity;
    $entity->delete();

    $this->t('The %label has been deleted!', [
      '%label' => $entity->label()
    ]);

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
