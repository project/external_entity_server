<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\external_entity_server\Contracts\ExternalEntityResourceInterface;

/**
 * Define the external entity cache invalidator entity form.
 */
class ExternalEntityCacheInvalidatorForm extends EntityForm {

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => 64,
      '#description' => $this->t(
        'External entity cache invalidate machine name.'
      ),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#required' => TRUE,
      '#default_value' => $entity->id(),
    ];

    $form['domain'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('Define all domains where external entity cache
        needs to be invalidated. All Drupal sites for these domains need to have
        the external entity module enable. <br/><strong>Note:</strong> Place each
        domain on a separate line.'),
      '#required' => TRUE,
      '#default_value' => implode("\n", $entity->getDomainList()),
    ];

    $form['resource'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Resource'),
      '#tree' => TRUE,
    ];
    $form['resource']['entity_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Types'),
      '#description' => $this->t('Select an entity type and bundle combination
        which should invalidate the external entity cache upon updating/deleting
        the same entity type/bundle on the host system.'),
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->getResourceEntityBundleOptions(),
      '#default_value' => $entity->resourceEntityTypes()
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    /** @var \Drupal\external_entity_server\Entity\ExternalEntityCacheInvalidator $entity */
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    $form_state->setRedirectUrl($entity->toUrl('collection'));

    return $status;
  }

  /**
   * Check if external entity cache invalidator exist.
   *
   * @param string $id
   *   The external entity cache invalidator ID.
   *
   * @return bool
   *    Return TRUE if cache invalidator exist; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exist(string $id): bool {
    $storage = $this->externalEntityCacheInvalidatorStorage();
    return $storage->getQuery()->count()->condition('id', $id)->accessCheck()->execute() !== 0;
  }

  /**
   * Get external entity resource storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResourceStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('external_entity_resource');
  }

  /**
   * Get resource entity bundle options.
   *
   * @return array|array[]
   *   An array of resource entity bundle options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResourceEntityBundleOptions(): array {
    $options = &drupal_static(__METHOD__, []);

    if (empty($options)) {
      foreach ($this->getResourceStorage()->loadMultiple() as $entity) {
        if (!$entity instanceof ExternalEntityResourceInterface) {
          continue;
        }
        $bundle = $entity->getTargetEntityBundle();
        $entity_type = $entity->getTargetEntityTypeId();
        $options[$entity_type]["{$entity_type}:{$bundle}"] = $bundle;
      }
    }

    return $options;
  }

  /**
   * External entity cache invalidate storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The external entity cache invalidator storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function externalEntityCacheInvalidatorStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('external_entity_cache');
  }

}
