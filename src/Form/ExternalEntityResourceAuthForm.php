<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Authentication\AuthenticationCollectorInterface;

/**
 * Define the external entity server authentication form.
 */
class ExternalEntityResourceAuthForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * @var \Drupal\Core\Authentication\AuthenticationCollectorInterface
   */
  protected $authenticationCollector;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    RouteBuilderInterface $route_builder,
    ConfigFactoryInterface $config_factory,
    AuthenticationCollectorInterface $authentication_collector
  ) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->authenticationCollector = $authentication_collector;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder'),
      $container->get('config.factory'),
      $container->get('authentication_collector')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'external_entity_server_authentication';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Authentication Provider'),
      '#description' => $this->t('Select the allowed authentication providers
        on which to use for the external entity resource endpoints.<br/>
        <strong>Note:</strong> The consumer will need to be configured to
        provide the authentication requirements per request.'
      ),
      '#options' => $this->getAuthenticationProviderOptions(),
      '#multiple' => TRUE,
      '#empty_value' => 'none',
      '#empty_option' => $this->t('-- None --'),
      '#default_value' => $this->authConfiguration()->get('provider'),
      '#states' => [
        'visible' => [
          ':input[name="anonymous_access"]' => [
            'checked' => FALSE,
          ],
        ],
        'required' => [
          ':input[name="anonymous_access"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];
    $form['anonymous_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow anonymous access'),
      '#description' => $this->t('If checked, then anonymous users will have
        access to all external entity resources regardless of authentication.'),
      '#default_value' => $this->authConfiguration()->get('anonymous_access'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    $config = $this->authConfiguration();
    $values = $form_state->cleanValues()->getValues();

    if (
      $config->get('provider') !== $values['provider']
      || $config->get('anonymous_access') !== $values['anonymous_access']
    ) {
      $config->setData($values)->save();
      $this->routeBuilder->rebuild();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'external_entity_server.resource.authentication',
    ];
  }

  /**
   * Get authentication provider options.
   *
   * @return array
   *   An array of authentication provider options.
   */
  protected function getAuthenticationProviderOptions(): array {
    $providers = array_keys(
      $this->authenticationCollector->getSortedProviders()
    );

    return array_combine($providers, $providers);
  }

  /**
   * Get resource authentication configuration instance.
   *
   * @return \Drupal\Core\Config\StorableConfigBase
   */
  protected function authConfiguration(): StorableConfigBase {
    return $this->config('external_entity_server.resource.authentication');
  }

}
