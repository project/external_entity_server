<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\external_entity_server\Contracts\ExternalEntityCacheInvalidatorInterface;

/**
 * Define the external entity cache invalidator.
 *
 * @ConfigEntityType(
 *   id = "external_entity_cache",
 *   label = @Translation("Cache Invalidator"),
 *   label_plural = @Translation("Cache Invalidators"),
 *   label_singular = @Translation("Cache Invalidator"),
 *   label_collection = @Translation("Cache Invalidators"),
 *   admin_permission = "administer external entity cache invalidator",
 *   config_prefix = "cache_invalidator",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "domain",
 *     "resource"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity_server\Form\ExternalEntityCacheInvalidatorForm",
 *       "edit" = "\Drupal\external_entity_server\Form\ExternalEntityCacheInvalidatorForm",
 *       "delete" = "\Drupal\external_entity_server\Form\ExternalEntityBaseDeleteForm",
 *       "default" = "\Drupal\external_entity_server\Form\ExternalEntityCacheInvalidatorForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\external_entity_server\Controller\ExternalEntityCacheInvalidatorListBuilder"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/external-entity/cache-invalidate",
 *     "add-form" = "/admin/config/services/external-entity/cache-invalidate/add",
 *     "edit-form" = "/admin/config/services/external-entity/cache-invalidate/{external_entity_cache}",
 *     "delete-form" = "/admin/config/services/external-entity/cache-invalidate/{external_entity_cache}/delete"
 *   }
 * )
 */
class ExternalEntityCacheInvalidator extends ConfigEntityBase implements ExternalEntityCacheInvalidatorInterface {

  /**
   * @var string
   */
  protected $domain;

  /**
   * @var array
   */
  protected $resource = [];

  /**
   * {@inheritDoc}
   */
  public function domain(): ?string {
    return $this->domain ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function resource(): array {
    return $this->resource;
  }

  /**
   * {@inheritDoc}
   */
  public function resourceEntityTypes(): array {
    return $this->resource['entity_types'] ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function getDomainList(): array {
    return isset($this->domain)
      ? array_map('trim', explode("\n", $this->domain))
      : [];
  }

}
