<?php

declare(strict_types=1);

namespace Drupal\external_entity_server\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldTypePluginManager;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\external_entity_server\Contracts\ExternalEntityResourceInterface;

/**
 * Define the external entity resource entity.
 *
 * @ConfigEntityType(
 *   id = "external_entity_resource",
 *   label = @Translation("External Entity Resource"),
 *   label_plural = @Translation("External Entity Resources"),
 *   label_singular = @Translation("External Entity Resource"),
 *   label_collection = @Translation("External Entity Resource"),
 *   admin_permission = "administer external entity resource",
 *   config_prefix = "entity_resource",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   config_export = {
 *     "id",
 *     "bundle",
 *     "properties",
 *     "entity_type_id"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\external_entity_server\Form\ExternalEntityResourceDefaultForm",
 *       "edit" = "\Drupal\external_entity_server\Form\ExternalEntityResourceDefaultForm",
 *       "delete" = "\Drupal\external_entity_server\Form\ExternalEntityBaseDeleteForm",
 *       "default" = "\Drupal\external_entity_server\Form\ExternalEntityResourceDefaultForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\external_entity_server\Controller\ExternalEntityResourceListBuilder"
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/external-entity/server/resource",
 *     "add-form" = "/admin/config/services/external-entity/server/resource/add",
 *     "edit-form" = "/admin/config/services/external-entity/server/resource/{external_entity_resource}",
 *     "delete-form" = "/admin/config/services/external-entity/server/resource/{external_entity_resource}/delete"
 *   }
 * )
 */
class ExternalEntityResource extends ConfigEntityBase implements ExternalEntityResourceInterface {

  use StringTranslationTrait;

  public const STATUS_FOUND = 'found';
  public const STATUS_MISSING = 'missing';


  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $bundle;

  /**
   * @var string
   */
  protected $entity_type_id;

  /**
   * @var array
   */
  protected $properties = [];

  /**
   * {@inheritDoc}
   */
  public function id(): ?string {
    return isset($this->entity_type_id, $this->bundle)
      ? "{$this->entity_type_id}.{$this->bundle}"
      : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * {@inheritDoc}
   */
  public function getTargetEntityBundle(): ?string {
    return $this->bundle;
  }

  /**
   * {@inheritDoc}
   */
  public function getTargetEntityTypeId(): ?string {
    return $this->entity_type_id;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllProperties(): array {
    return NestedArray::mergeDeep(
      $this->getDefaultProperties(),
      $this->getExposedProperties()
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultPropertyIds(): array {
    return ['id', 'uuid', 'label'];
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceDependencies(): array {
    $dependencies = [];

    foreach ($this->getPropertyReferenceFieldDefinitions() as $definition) {
      if ($target_type = $definition->getSetting('target_type')) {
        if (!isset($dependencies[$target_type])) {
          $dependencies[$target_type] = [];
        }
        $handler_settings = $definition->getSetting('handler_settings');
        $dependencies[$target_type] += $handler_settings['target_bundles'] ?? [];
      }
    }

    return $dependencies;
  }

  /**
   * {@inheritDoc}
   */
  public function getResourceDependencyStatus(): array {
    $status = [];
    $storage = $this->getEntityStorage();

    foreach ($this->getResourceDependencies() as $type => $dependencies) {
      foreach ($dependencies as $bundle) {
        $id = "{$type}.{$bundle}";
        $result = $storage->getQuery()
          ->accessCheck()
          ->count()
          ->condition('id', $id)
          ->execute();
        $key = $result === 0 ? static::STATUS_MISSING : static::STATUS_FOUND;
        $status[$key][] = $id;
      }
    }

    return $status;
  }

  /**
   * {@inheritDoc}
   */
  public function getPropertyFieldDefinitions(
    string $interface = NULL
  ): array {
    $definitions = [];
    $field_definitions = $this->getFieldDefinitions();

    foreach ($this->getAllProperties() as $field_name => $info) {
      if (!isset($info['name'], $field_definitions[$field_name])) {
        continue;
      }
      $definition = $field_definitions[$field_name];

      if (
        isset($interface)
        && !is_subclass_of($definition->getClass(), $interface)
      ) {
        continue;
      }

      $definitions[$info['name']] = $definition;
    }

    return $definitions;
  }

  /**
   * Get property reference field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of property reference field definitions.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPropertyReferenceFieldDefinitions(): array {
    return $this->getPropertyFieldDefinitions(
      EntityReferenceFieldItemListInterface::class
    );
  }

  /**
   * {@inheritDoc}
   */
  public function label(): string {
    $target_definition = $this->getTargetEntityType();
    $target_bundle_label = $this->getTargetEntityBundleInfo()['label'] ?? NULL;
    return "{$target_definition->getLabel()}/{$target_bundle_label}";
  }

  /**
   * {@inheritDoc}
   */
  public function exists(): bool {
    return (bool) $this->getEntityStorage()
      ->getQuery()
      ->condition('id', $this->id())
      ->accessCheck()
      ->execute();
  }

  /**
   * Get default external entity definition properties.
   *
   * @return array
   *   An array of default properties.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDefaultProperties(): array {
    $properties = [];
    $definition = $this->getTargetEntityType();

    foreach ($this->defaultPropertyIds() as $key) {
      if (!$definition->hasKey($key)) {
        continue;
      }
      $field_name = $definition->getKey($key);
      $properties[$field_name]['name'] = $key;
    }

    return $properties;
  }

  /**
   * Get exposed external entity definition properties.
   *
   * @return array
   *   An array of exposed properties.
   */
  protected function getExposedProperties(): array {
    return array_filter($this->getProperties(), static function ($value) {
      return isset($value['exposed']) && $value['exposed'];
    });
  }

  /**
   * Get target entity type field definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions.
   */
  protected function getFieldDefinitions(): array {
    return $this->entityFieldManager()->getFieldDefinitions(
      $this->getTargetEntityTypeId(),
      $this->getTargetEntityBundle()
    );
  }

  /**
   * Get the target entity bundle info.
   *
   * @return array
   *   An array of entity bundle info.
   */
  protected function getTargetEntityBundleInfo(): array {
    $bundle_info = $this->entityTypeBundleInfo()->getBundleInfo(
      $this->getTargetEntityTypeId()
    );

    return $bundle_info[$this->getTargetEntityBundle()] ?? [];
  }

  /**
   * Get target entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *    The entity type definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTargetEntityType(): EntityTypeInterface {
    return $this->entityTypeManager()
      ->getDefinition($this->getTargetEntityTypeId());
  }

  /**
   * Entity field manager.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity field manager service.
   */
  protected function entityFieldManager(): EntityFieldManagerInterface {
    return \Drupal::service('entity_field.manager');
  }

  /**
   * Field type manager.
   *
   * @return \Drupal\Core\Field\FieldTypePluginManager
   *   The field type manager service.
   */
  protected function fieldTypeManager(): FieldTypePluginManager {
    return \Drupal::service('plugin.manager.field.field_type');
  }

  /**
   * Get entity storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage($this->getEntityTypeId());
  }

}
