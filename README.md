External Entity Server
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

The External Entity Server module was developed to expose entity resources via an API endpoint. These resources would then be consumed by other Drupal sites that need to display the same content.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/external_entity_server

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/external_entity_server

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

* [External Entity](https://www.drupal.org/project/external_entity):
  This module provides a way to consume the entity data resources from other various sources.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

More to come after we finalize/stabilize the codebase.

MAINTAINERS
-----------

Current maintainers:
* Travis Tomka (droath) - https://www.drupal.org/u/droath

This project has been sponsored by:

* Corning Museum of Glass (https://home.cmog.org)

